---
gitea: none
include_toc: true
---

# Introducing [Nudie.Social](https://pl.nudie.social/) and the Fediverse

To find out what Fediverse, and Nudie.Social are about, _read on!_

# What is Nudie.Social?

Nudie.Social is a Twitter-style social network, dedicated to naturists, nudists, or anyone interested in clothing-optional living. Similar to Twitter, members of Nudie.Social can post short updates under their profiles. `Tech Jargon:` Twitter and Nudie.Social are both classified as / _Micro-blogging_ social networks.

## How is it like Twitter?

You post relatively-short status updates, and you can see a stream of updates from members/users you follow. A status update is called a "post" or "toot", similar to a tweet on Twitter.

Following other users work just like Twitter. Once you follow another users, their posts will start appearing in your home timeline. p.s. Timeline = stream of posts. (More on this below)

You can _re-post_ or _boost_ an interesting post, so your followers can see it - Just like you can _retweet_ a tweet in Twitter. On Nudie.Social there is also a quote-re-post function similar to quote retweet on Twitter.

Nudie.Social also supports _hashtags_, which are words prefixed by #, like #BodyPositive or #NudeBeach . users can click on a hashtag to search for other posts containing that tag.

## How is it NOT like Twitter?

Unlike Twitter, Nudie.Social is 100% community-run, not-for-profit. There is no shareholder or advertiser. Nudie.Social's Community Guidelines are shaped by their members, not by corporate interests. Nudie.Social does not display advertisement, does not analyse member's activities, and does not sell member's data.

Nudie.Social has a nudity friendly, clothing-optional Terms of Service / Community Guidelines. Unlike Twitter, posting contents related to clothes-free living, non-sexual nudity, naturism, or nudism on Nudie.Social is encouraged, and will not result in account suspension.

Nudie.Social put you, the members, in control. We do not analyse the keywords in your posts, do not run "algorithms / AI bots" to show you recommendations, and we do not shovel sponsored contents down your throat!

Twitter operates as a "walled-garden", meaning that Twitter users can only interact with other Twitter users within the Twitter network.

Unlike Twitter, Nudie.Social is connected to a vast, federated network consists of thousands of communities. This network is called the Fediverse, `FEDerated unIVERSE`.

# What is the Fediverse?

The Fediverse is a vast network of communities that communicate via the ActivityPub or OStatus protocols. (Generally speaking, unless you're making or editing software to interact with the fediverse, you don't need to know what those are.) Each of these communities has different theme, such as creative arts, technology, digital privacy and many more. Nudie.Social is just one of the 4000-strong communities within this global network. As a member of Nudie.Social, you are free to connect to members of other communities, and participate in various topics and discourses.

Each community can also be called an "instance" or a "server". These terms are used interchanaglablely within this guide.

For the tech savvy readers: There are lots of different kinds of server software on the Fediverse, like Pixelfed, Akkoma, Pleroma, CalcKey, Misskey, Mastodon, PeerTube, or WriteFreely. Nudie.Social runs on the Akkoma server software. Mastodon is currently the most popular server software.

__Good to know:__ Mastodon is so popular as of 2022, many people are unaware of the many different Fediverse software!

## How does the Fediverse work like email?

Each Fediverse community is independent but networked, like email servers. If you sign up for an email account on gmail.com, you don't automatically have an account on hotmail.com or aol.com, but you can send and receive messages to and from users on hotmail.com and aol.com.

Likewise, if you sign up for an account on Nudie.Social, that doesn't make an account for you on every other communities, but you can talk to users from other communities and they can talk to you. Following works the same way, you can follow users from other communities, and they can follow you.

Having said that, you are free to create accounts on multiple instances if you want to talk about different things separately. You could have an account on https://cybre.space/ to talk about technology, an account on https://elekk.xyz/ to talk about gaming, and an account on https://mstdn.social/ for general chatter. You have to sign into each account separately and keep each open in a separate browser tab or window.

# What set Nudie.Social apart from other "naturist" web sites?

As of December 2022, there are several naturist / nudist social networks on the 'net. Almost all of them are free to join, some have free-tier and paid-tier memberships.

We, Nudie.Social, are different from those web sites in the following ways:

1. We are not a "walled garden" or an "island" - Nudie.Social are connected to thousands of communities on the Fediverse. The conversations on Nudie.Social will never been stale - So many communities and folks to connect to!
1. We operate as a community service: For the community, by the community. Members have a say in how Nudie.Social should evolve
1. We have Community Guidelines that cater for naturists, nudists, non-sexual nudity, clothing-optional living, etc. We always welcome member's feedback and improvements to our Community Guidelines
1. We moderate actively, to ensure everyone has a great experience; We strive to be a safe space, and do not tolerate harassment
1. We focus on Twitter-style micro-blogging, and not try to be forums / dating site / "nudies facebook" / everything
1. We are an open, non-commercial, not-for-profit social network, and will never implement paid-tier membership
1. We do not accept commercial sponsorship. In our long term plan, we may accept voluntary donations if server operation cost reached a certain amount

## Who created Nudie.Social?

Nudie.Social was founded by [Nude Ninja](https://nudeninja.blog/) in 2018. Here's Ninja's video self-introduction: (NSFW, nudity) https://peertube.slat.org/w/vtyHDMQNbXQNsmtCU2LDHK

As of December 2022, Ninja is the sole operator of Nudie.Social.

## Why did Ninja create Nudie.Social?

Ninja strongly believes that mainstream social networks are doing a disservice to naturists and nudists around the world. Commercial social networks' interests do not, and will not, align with naturist values.

Ninja created Nudie.Social to explore the possiblity to run a sustainable, non-commercial, clothing optional social network.

Feel free to reach out to Ninja and discuss this topic in-depth.

# Can I have a look around, before I join?

Yes, you're welcome! Click the following link to see the public posts from Nudie.Social's members https://pl.nudie.social/main/public

Follow this link to see a sample of posts from Fediverse communities: https://pl.nudie.social/main/all . Most of these posts are probably about topics other than naturism or nudism.

If you're familiar with Twitter, you may know that there are thousands of nudies online. Nudie.Social is a much younger community, so there are far less people around. But that's okay - Good things take time!

## Okay, I'll give it a go. How do I join Nudie.Social?

Head over to the registration page: https://pl.nudie.social/registration , have a read of the [Terms of Service and Community Participation Guidelines](https://codeberg.org/Nudie.Social/tos-community-guidelines/src/branch/master/nudie_social_tos.md).

If you agree to the TOS, choose a username, password and fill in your email address. We do not share your email address to any 3rd party. The email address serves only 2 purposes:

1. Prevent bots from creating many accounts
1. Allow members to reset their password just in case

Moreover, Nudie.Social do not send any email to users. You will not receive emails like, "what's happening here", "new posts" alerts, or any form of spam email. Consider using a mobile app if you'd like to receive notifications (More on this later)

After submitting the registration form, you'll soon receive an email with an unique verification link. Please click the link and log in with your username and password.

## Okay, I'm logged into Nudie.Social, now what?

First thing first, we strongly recommend you to read our community guidelines, if you've not already done so. Already read it? That's great, _let's go!_

__New!__ Video "Survival Guide" of Nudie.Social and Fediverse: https://peertube.slat.org/w/m9bG7STNm6egB7VQ9i1iNa

Nudie.Social does not have any algorithm or "AI" to generate follow recommendations. Therefore, it's up to _you_ to discover other members/users, and build up your own follows.

Before you interact with other users, we strongly recommend you to:

1. Update your profile - Upload an avatar, write a short description about yourself. You can do that by clicking the "gear" icon on the top-right corner of the page, then click the Profile tab
1. Send a DM to moderator to say hello, and request a list of naturists on Fediverse (See below "How do I get help?" for how to send a DM)
1. Explore different "timelines" (See below)

## Suggested activities for new Members 

1. Find some cool people on the timelines, Start some discussions
1. Write a short introduction post, with the hashtag `#introduction`
1. Optional: Install a mobile app

## What are "Timelines" and "Known Network"?

__New!__ Video "Survival Guide" of Nudie.Social and Fediverse: https://peertube.slat.org/w/m9bG7STNm6egB7VQ9i1iNa

Timeline is a stream of status updates (posts) from multiple users. If you've used Twitter, you'd notice there is only one "stream" of tweets - Tweets from the people you're following, plus any sponsored content / ads.

In Nudie.Social, your _Home Timeline_ is the rough equivalent of the Twitter stream - Minus the annoying ads, sponsored tweets, and recommendations. In Nudie.Social's Home Timeline, you'll see the posts and re-posts from users you follow, who may be from Nudie.Social, or other Fediverse communities.

As mentioned before, users are in control of who to follow - It's totally normal to have an empty home timeline.

In addition, there are three more streams of posts on Nudie.Social: _Public Timeline, Bulle Timeline, and Known Network._

The _Public Timeline_ is every post with a public status, posted by members of Nudie.Social, with the exception of replies. (A reply is any posts posted in response to another post - NOT any post that simply mentions another user!)

Think of the Public Timeline as Nudie.Social's "Town Centre" - On this timeline, you can read what is happening on Nudie.Social, without having to follow each Nudie.Social user.

The _Known Network_ contains posts with a public status, posted by any user that the Nudie.Social instance knows about, even from other Fediverse communities. The Nudie.Social instance discovers and fetches posts from other Fediverse communities dynamically, based on user interactions such as like, re-post/boost, reply, etc.

Due to the global scale of the Fediverse network, the Known Network timeline is high traffic and can be hard to make sense of. Be careful!

Last but not the least - _Bubble Timeline_, which is a curated list of public posts from instances similar to Nudie.Social. Similar to Known Network, but much less traffic.

# How do I find interesting people and naturists to follow/connect?

Fediverse doesn't have algorithm to recommend content. Discovering content/users an exercise to the Fediverse users. In contrast, corporate-run social networks typically does very well in recommending things to user, in the expense of analysing user contents (privacy violation) and reinforcing users' bias.

Here's some recommended way to find interesting people to follow:

## Look at the following/follower lists of other Nudie.Social members

This is both a good way to find other users, as well as exploring other instances on Fediverse - The second part of a Fediverse username is always the instance's name.

This is best done on a desktop computer as you'll want to open up a few browser tabs and look at different instances. Due to the way federation works, often you may see an user having very few posts - Always a good idea to browse the user's profile on their instance to double check, you may find more cool people there!

## Use Nudie.Social's search bar

Nudie.Social search bar is quite capable - Not as fast as Google, but can still turn up relevant posts. It can search hashtags, too!

p.s. If you find a cool hashtag, feel free to follow it!

## https://fediverse.info/

Great search engine for finding people using hashtags in their profiles: https://fediverse.info/

## Trunk - People Directory

A web site to connect new Fediverse users to real people, based on category of interests: https://communitywiki.org/trunk/

There's a __Naturists and Nudists__ category. You can request to be added in the category

https://communitywiki.org/trunk/grab/Nudists%20and%20Naturists

## Get the full conversation

When interacting with others (e.g. replying to someone in a thread), open up the thread in their instance is also a good idea. Again, the way federation works is that threads may or may not be fetched from remote instances in their entirety.

# How do I get help?

The best way to get help is: Ask! Very simple. Post some question, and you should receive response from other members in no time!

# About this guide

This guide is adapted from the excellent [Mastodon Guide](https://github.com/joyeusenoelle/GuideToMastodon) by `@noelle@chat.noelle.codes`, with permission.
